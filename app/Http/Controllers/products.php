<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Session;
use App\Models\Product;
use Illuminate\Http\Request;
class products extends Controller

{
        /////// View Products Controller Start //////////////
        public function viewproducts()
            {
                $datasession=Session::get('mysession');
                $data=Product::where('admin_vender_id', '=', $datasession)->get();
                return view('pages/viewproducts',['products'=>$data]);
            }
        /////// View Products Controller End //////////////


        /////// Add Products Controller Start //////////////

        public function addproductsform(Request $reqs)
            {
                return view('pages/addproducts');
            }


        function addproducts(Request $reqst)
            {
                $product=new Product;
                $product->admin_vender_id= $reqst->admin_vender_id;
                $product->productName= $reqst->productname;
                $product->brand= $reqst->Brand;
                $product->Category= $reqst->category;
                $product->price= $reqst->Price;
                $product->quantity= $reqst->Quantity;
                $product->save();
                return redirect('viewproducts');
            }
     /////// Add Products Controller End //////////////




                function productsedit($id)
                {
                    $data= Product::find($id);
                    return view('pages/editproducts',['data'=>$data]);
                }


}
