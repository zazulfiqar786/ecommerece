<?php

namespace App\Http\Controllers;

use App\Models\User as ModelsUser;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Foundation\Auth\User as AuthUser;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    function venders()
    {
        $data= User::get();
      return view('venders',['data'=>$data]);

    }

    function venderbar()
    {
        $data= User::get();
        return view('venderbar',['data'=>$data]);

    }

    function venderbarfetch($id)
    {
        $dataupdate= User::find($id);
        return view('venderbarfetch',['venderbar'=>$dataupdate]);
    }

    function vendersupdate($id)
    {
        // $data= User::where('category','Vender')->get();
        $dataupdate= User::find($id);
        return view('vendersupdate',['data'=>$dataupdate]);
    }


    function vendersupdatedata(Request $reqs){
        $data2= User::find($reqs->id);
        $data2-> firstname=$reqs->firstname;
        $data2-> lastname=$reqs->lastname;
        $data2-> phone=$reqs->phone;
        $data2-> city=$reqs->city;
        $data2-> street=$reqs->street;
        $data2-> zipcode=$reqs->zipcode;
        $data2-> category=$reqs->category;
        $data2-> status=$reqs->status;
        $data2-> venderbar=$reqs->venderbar;
        $data2-> save();
        return redirect('venders');

    }






      public function Logout(Request $request)
      {
          $request->session()->flush();

          return redirect('/home');
      }

}
