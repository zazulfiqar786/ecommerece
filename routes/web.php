<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;



Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/sidebar', [App\Http\Controllers\HomeController::class, 'sidebar'])->name('sidebar');
Route::get('/headerbar', [App\Http\Controllers\HomeController::class, 'headerbar'])->name('headerbar');
Route::get('/venders', [App\Http\Controllers\HomeController::class, 'venders'])->name('venders');
Route::get('/vendersupdate/{id}', [App\Http\Controllers\HomeController::class, 'vendersupdate'])->name('vendersupdate');
Route::post('vendersupdate', [App\Http\Controllers\HomeController::class, 'vendersupdatedata'])->name('vendersupdatedata');
Route::get('/logout', [App\Http\Controllers\HomeController::class, 'logout'])->name('logout');
// Route::get('/venderbarfetch', [App\Http\Controllers\HomeController::class, 'venderbarfetch'])->name('venderbarfetch');

Route::get('/viewproducts', [App\Http\Controllers\Products::class, 'viewproducts'])->name('pages/viewproducts');
Route::get('/addproducts', [App\Http\Controllers\Products::class, 'addproductsform'])->name('pages/addproducts');
Route::post('/submit', [App\Http\Controllers\Products::class, 'addproducts'])->name('pages/addproducts');




Route::get('/editproducts/{id}', [App\Http\Controllers\Products::class, 'productsedit']);
