@extends('layouts.app')


@section('content')
{{session(['mysession' =>  Auth::user()->id])}}
    <head>
        <meta charset="utf-8">

        <title>Ecommerece Site</title>

        <meta name="description" content="ProUI is a Responsive Bootstrap Admin Template created by pixelcave and published on Themeforest.">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">
         <link rel="shortcut icon" href="img/favicon.png">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/plugins.css')}}">
        <link rel="stylesheet" href="{{ asset('css/main.css')}}">
        <link rel="stylesheet" href="{{ asset('css/themes.css')}}">

        <script src="{{ asset('js2/vendor/modernizr.min.js')}}"></script>
    </head>
    <body>

        <div id="page-wrapper">
            <div class="preloader themed-background">
                <h1 class="push-top-bottom text-light text-center"><strong>Pro</strong>UI</h1>
                <div class="inner">
                    <h3 class="text-light visible-lt-ie10"><strong>Loading..</strong></h3>
                    <div class="preloader-spinner hidden-lt-ie10"></div>
                </div>
            </div>

            <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">

                @include('sidebar')

                <div id="main-container">

                    <div id="page-content">
                        <!-- Start  Header -->
                        @include('headerbar')
                        <!-- END  Header -->

                        <!-- Start  Body -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">

                                            Add Products

                                        </h5>
                                    </div>
                                    <br>

                                <form method="post" action="productsupdate">


                                        @csrf
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Product Name</label>
                                        <input type="hidden" class="form-control" value="{{$data['id']}}"  name="admin_vender_id" >

                                        <div class="col-sm-10">
                                        <input type="text" class="form-control"  name="productname" value="{{$data['productName']}}" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Brand</label>
                                        <div class="col-sm-10">
                                        <input type="text" class="form-control"  name="Brand" value="{{$data['brand']}}" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Category</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control"  name="category" value="{{$data['category']}}" >
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Price</label>
                                        <div class="col-sm-10">
                                        <input type="number" class="form-control"  name="Price" value="{{$data['price']}}" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Quantity</label>
                                        <div class="col-sm-10">
                                        <input type="number" class="form-control"  name="Quantity" value="{{$data['quantity']}}" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-default">Update</button>
                                        </div>
                                    </div>

                                </form>


                                </div>
                            </div>
                        </div>
                        <!-- End  Body -->

                    </div>
                </div>

            </div>

        </div>

        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        <script src="{{ asset('js2/vendor/jquery.min.js')}}"></script>
        <script src="{{ asset('js2/vendor/bootstrap.min.js')}}"></script>
        <script src="{{ asset('js2/plugins.js')}}"></script>
        <script src="{{ asset('js2/app.js')}}"></script>


    </body>
</html>
@endsection
