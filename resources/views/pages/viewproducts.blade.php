@extends('layouts.app')


@section('content')

    <head>
        <meta charset="utf-8">

        <title>Ecommerece Site</title>

        <meta name="description" content="ProUI is a Responsive Bootstrap Admin Template created by pixelcave and published on Themeforest.">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">
         <link rel="shortcut icon" href="img/favicon.png">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/plugins.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/themes.css">

        <script src="js2/vendor/modernizr.min.js"></script>
    </head>
    <body>

        <div id="page-wrapper">
            <div class="preloader themed-background">
                <h1 class="push-top-bottom text-light text-center"><strong>Pro</strong>UI</h1>
                <div class="inner">
                    <h3 class="text-light visible-lt-ie10"><strong>Loading..</strong></h3>
                    <div class="preloader-spinner hidden-lt-ie10"></div>
                </div>
            </div>

            <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">

                <!-- Main Sidebar -->


                @include('sidebar')



                <!-- END Main Sidebar -->


                <div id="main-container">

                    <div id="page-content">
                        <!-- Dashboard Header -->
                        <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
                        @include('headerbar')
                        <!-- END Dashboard Header -->

                        <!-- Mini Top Stats Row -->
                        <div class="row">

                            <div class="table-responsive">
                                <table class="table table-vcenter table-striped">
                                    <thead>

                                        <tr>
                                            <th style="width: 150px;" class="text-center"><i class="gi gi-user"></i></th>
                                            <th class="text-center">Product Name</th>
                                            <th class="text-center">Brand</th>
                                            <th class="text-center">Category</th>
                                            <th class="text-center">Price </th>
                                            <th class="text-center">Quantity </th>
                                            <th style="width: 150px;" >Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>


                                        @foreach($products as $product)
                                        <tr>

                                            <td class="text-center">{{$product['admin_vender_id']}}</td>
                                            <td class="text-center">{{$product['productName']}}</td>
                                            <td class="text-center">{{$product['brand']}}</td>
                                            <td class="text-center">{{$product['category']}}</td>
                                            <td class="text-center">{{$product['price']}}</td>
                                            <td class="text-center">{{$product['quantity']}}</td>
                                            <td> <div class="btn-group btn-group-xs">
                                                <a  data-toggle="tooltip" title="view" class="btn btn-default"><i class="fa fa-eye"></i></a>
                                                <a href='editproducts/{{$product['id']}}'  data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                                                <a href="javascript:void(0)" data-toggle="tooltip" title="Delete" class="btn btn-danger"><i class="fa fa-times"></i></a>
                                            </div>
                                        </td>
                                        </tr>
                                        @endforeach


                                    </tbody>
                                </table>
                            </div>



                        </div>

                    </div>
                </div>

            </div>

        </div>

        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        <script src="{{ asset('js2/vendor/jquery.min.js')}}"></script>
        <script src="{{ asset('js2/vendor/bootstrap.min.js')}}"></script>
        <script src="{{ asset('js2/plugins.js')}}"></script>
        <script src="{{ asset('js2/app.js')}}"></script>


    </body>
</html>
@endsection
