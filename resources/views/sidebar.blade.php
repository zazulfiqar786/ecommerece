<button  href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
    <i class="fa fa-bars fa-fw navbtn"></i>
 </button>
<div id="sidebar">
    <!-- Wrapper for scrolling functionality -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Brand -->



            <!-- END Brand -->

            <!-- User Info -->
            <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                <div class="sidebar-user-avatar">
                    <a href="page_ready_user_profile.html">
                        <img src="{{ asset('img/placeholders/avatars/avatar2.jpg')}}" alt="avatar">
                    </a>
                </div>
                <div class="sidebar-user-name" >


                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))

                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>

                            @endif

                            @if (Route::has('register'))

                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>

                            @endif
                        @else
                            <span class="nav-item dropdown">
                                <span id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Profile
                                </span>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </span>
                        @endguest


                </div>

            </div>
            <!-- END User Info -->

            <!-- Theme Colors -->
            <!-- Change Color Theme functionality can be found in js2/app.js - templateOptions() -->

            <!-- END Theme Colors -->

            <!-- Sidebar Navigation -->
            <ul class="sidebar-nav">
                <li>
                    <a href="#" class="sidebar-nav-menu">
                        <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
                        <i class="gi gi-shopping_cart sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Sales</span></a>
                    <ul>
                        <li>
                            <a href="page_ecom_dashboard.html">Orders</a>
                        </li>
                        <li>
                            <a href="page_ecom_orders.html">Shipment</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="#" class="sidebar-nav-menu">
                        <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
                        <i class="gi gi-book sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Catalog</span></a>
                    <ul>
                        <li>
                            <a href="page_ecom_orders.html">Categories</a>
                        </li>
                        <li>
                            <a href="/addproducts" > Add Products</a>
                        </li>
                        <li>
                            <a href="/viewproducts" >View Products</a>
                        </li>


                    </ul>
                </li>

                <li>
                    <a href="#" class="sidebar-nav-menu">
                        <i class="gi gi-user sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Customers</span></a>
                </li>

                @if(Auth::user()->venderbar==1)
                <li >
                    <a href="/venders">
                        <i class="gi gi-user sidebar-nav-icon"></i>
                        <span class="sidebar-nav-mini-hide">Vendors</span></a>
                </li>
                @else

                @endif

            </ul>
            <!-- END Sidebar Navigation -->

            <!-- Sidebar Notifications -->

            <!-- END Sidebar Notifications -->
        </div>
        <!-- END Sidebar Content -->
    </div>
    <!-- END Wrapper for scrolling functionality -->
</div>
